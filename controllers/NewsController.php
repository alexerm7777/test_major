<?php

    namespace app\controllers;

    use yii\web\UploadedFile;
    use app\models\UploadForm;
    use Yii;
    use app\models\News;
    use yii\data\ActiveDataProvider;
    use yii\web\Controller;
    use yii\web\NotFoundHttpException;
    use yii\filters\VerbFilter;

    /**
     * NewsController implements the CRUD actions for News model.
     */
    class NewsController extends Controller
    {
        /**
         * @inheritdoc
         */
        public function behaviors()
        {
            return [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ];
        }

        /**
         * Lists all News models.
         * @return mixed
         */
        public function actionIndex()
        {
            $news = new News();
            return $this->render('index', [
                'news' => News::find()->all(),
            ]);
        }

        /**
         * Displays a single News model.
         * @param integer $id
         * @return mixed
         * @throws NotFoundHttpException if the model cannot be found
         */
        public function actionView($id)
        {
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }

        /**
         * Creates a new News model.
         * If creation is successful, the browser will be redirected to the 'view' page.
         * @return mixed
         */
        public function actionCreate()
        {
            $model = new News();


            $this->handleNewsSave($model);

            return $this->render('create', [
                'model' => $model
            ]);
        }

        /**
         * Updates an existing News model.
         * If update is successful, the browser will be redirected to the 'view' page.
         * @param integer $id
         * @return mixed
         * @throws NotFoundHttpException if the model cannot be found
         */
        public function actionUpdate($id)
        {
            $model = $this->findModel($id);

            $this->handleNewsSave($model);

            return $this->render('update', [
                'model' => $model,
            ]);
        }


        public function actionDelete($id)
        {

            if($this->findModel($id)->delete())
            {
                return true;
            }

            return false;
        }

        /**
         * Finds the News model based on its primary key value.
         * If the model is not found, a 404 HTTP exception will be thrown.
         * @param integer $id
         * @return News the loaded model
         * @throws NotFoundHttpException if the model cannot be found
         */
        protected function findModel($id)
        {
            if (($model = News::findOne($id)) !== null) {
                return $model;
            }

            throw new NotFoundHttpException('The requested page does not exist.');
        }

        protected function handleNewsSave(News $model)
        {
            if ($model->load(Yii::$app->request->post()))
            {
                $model->image = UploadedFile::getInstance($model, 'image');

                if ($model->validate())
                {
                    if ($model->image)
                    {
                        $filePath = 'uploads/' . $model->image->baseName . time() . '.' . $model->image->extension;
                        if ($model->image->saveAs($filePath))
                        {
                            $model->image = $filePath;
                        }
                    }

                    if ($model->save(false))
                    {
                        return $this->redirect(['view', 'id' => $model->id]);
                    }
                }
            }
        }

    }
