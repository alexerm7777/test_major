<?php
    /**
     * Created by PhpStorm.
     * User: Dmitriy
     * Date: 08.03.2018
     * Time: 03:01
     */

    namespace app\models;

    use yii\base\Model;
    use yii\web\UploadedFile;

    class UploadForm extends Model
    {
        /**
         * @var UploadedFile
         */
        public $image;

        public function rules()
        {
            return [
                [['image'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg'],
            ];
        }

        public function upload($id)
        {
            if ($this->validate()) {
                $this->image->saveAs('uploads/news/'.$id.'/'. $this->image->baseName . '.' . $this->image->extension);
                return true;
            } else {
                return false;
            }
        }
    }