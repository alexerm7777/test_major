<?php

    use yii\helpers\Html;


    /* @var $this yii\web\View */

    $this->title = 'Новости';
    $this->params['breadcrumbs'][] = $this->title;


?>
<div class="news-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить новость', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php
        if (empty($news)) {
            echo 'Новостей нету';
        }

        /** @var array $news */
        foreach ($news as $item) {
            ?>

            <div class="row" id="news<?= $item->id ?>">

                <div class="col-md-12 ">
                    <h1><a href="<?= \yii\helpers\Url::to(['news/view', 'id' => $item->id]) ?>"><?= $item->title ?></a></h1>
                    <img width="200px" style="margin: 0 5px 5px 0;"
                         src="<?= \Yii::getAlias('@web') . '/' . $item->image ?>" alt="<?= $item->title ?>"
                         class="pull-left img-responsive thumb margin10 img-thumbnail">
                    <input type="button" class="btn btn-danger delete pull-right" data-id="<?= $item->id ?>"
                           value="Удалить">

                    <article>
                        <p><?= \yii\helpers\StringHelper::truncate($item->description, 150,
                                '<a class="btn btn-blog " href="' . \yii\helpers\Url::to([
                                    'news/view',
                                    'id' => $item->id
                                ]) . '">Читать далее</a>'); ?>
                        </p>
                    </article>

                </div>
            </div>


            <?php
        }
    ?>


</div>
